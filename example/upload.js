const ci = require('miniprogram-ci');
const path = require('path');
const config = require('./project.config.json');

const project = new ci.Project({
  appid: config.appid,
  type: 'miniProgram',
  projectPath: path.join(__dirname, './'),
  privateKeyPath: path.join(__dirname, './private.key'),
  ignores: ['node_modules/**/*'],
});

ci.upload({
  project,
  version: '1.3.4',
  desc: 'desc',
  setting: config.setting,
});
